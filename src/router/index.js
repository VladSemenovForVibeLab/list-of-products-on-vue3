// Импортируем необходимые функции и компоненты из Vue Router
import {createRouter, createWebHistory} from 'vue-router'
import Main from "@/pages/Main.vue";
import BuckerCart from "@/pages/BuckerCart.vue";
// Создаем экземпляр роутера с помощью функции createRouter
const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path:'/',  // Путь к корневой странице
      component:Main // Связываем этот путь с компонентом Main
    },
    {
      path:'/bucket',  // Путь к странице корзины
      component:BuckerCart // Связываем этот путь с компонентом BuckerCart
    }
  ]
})
export default router // Экспортируем созданный роутер для использования в приложении
